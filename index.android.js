import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import AppNavigator from './src/AppNavigator.js';
import CoverPage from './src/CoverPage.js';

class calc_app extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bShowCover: true
    }
  }

  render() {
    if (this.state.bShowCover) {
      return (<CoverPage />);
    } else {
      return (<AppNavigator />)
    }
  }

  componentDidMount() {
    var _this = this;
    setTimeout(function () {
      _this.setState({ bShowCover: false });
    }, 2000);
  }
}

AppRegistry.registerComponent('calc_app', () => calc_app);
