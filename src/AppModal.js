import React, {Component} from 'react';
import {TouchableOpacity, Image, ScrollView, Modal} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';

class AppModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            children: props.children,
            onRequestClose: props.onRequestClose,
            visible: props.visible
        }
    }



    render() {
        return (
            <Modal
                style={styles.modalStyle}
                animationType={"slide"}
                transparent={true}
                visible={this.state.visible}
                onRequestClose={this.state.onRequestClose}>
                <ScrollView
                    style={{
                        height: Style.DEVICE_HEIGHT,
                        width: Style.DEVICE_WIDTH,
                        backgroundColor: 'rgba(0,0,0,.7)'
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{
                            position: 'absolute',
                            right: Style.TOP_BAR_HEIGHT / 3,
                            top: Style.TOP_BAR_HEIGHT / 3
                        }}
                        onPress={this.state.onRequestClose}>
                        <Image
                            source={require("./assets/close-icon.png") }
                            style={{
                                height: Style.TOP_BAR_HEIGHT / 3,
                                width: Style.TOP_BAR_HEIGHT / 3,
                                resizeMode: 'contain'
                            }}/>
                    </TouchableOpacity>
                    {this.state.children}
                </ScrollView>
            </Modal>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
    }
)

export default AppModal;