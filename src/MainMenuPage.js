import React, { Component } from 'react';
import {Platform, Animated, ScrollView, Modal, Text, TouchableOpacity, Image, View} from 'react-native';
import Style from './Style.js';
import AppModal from './AppModal.js';
import MainButton from './MainButton.js';
import {MediaQueryStyleSheet} from 'react-native-responsive';

class MainMenuPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navigator: props.navigator,
            modalVisible: false,
        }
    }

    onCalculatorPress() {
        this.state.navigator.push({
            id: 'calculator',
            title: 'Calculators'
        });
    }

    onAboutPress() {
        this.state.navigator.push({
            id: 'about',
            title: 'About'
        });
    }

    onSubscribePress() {
        //TODO
    }

    setModalVisible(b) {
        this.setState({ modalVisible: b });
    }

    onDisclaimerPress() {
        this.setModalVisible(true);
    }

    renderModal() {
        if (!this.state.modalVisible)
            return;

        return (
            <AppModal
                visible={this.state.modalVisible}
                onRequestClose={() => { this.setModalVisible(false) } }>
                <View style={styles.modalStyle}>
                    <Text style={styles.modalTitle}>Warning!</Text>

                    <Text style={styles.modalTextStyle}>The information contained in this application is general in nature and not intended as personal financial advice.
                        Useres should seek professional advice before making any decisions based on any of the information contained herein.
                        The rates contained in the various calculators are current as at 1st July 2016.</Text>
                </View>
            </AppModal>);
    }

    render() {
        return (
            <View style={styles.mainPageStyle}>
                {this.renderModal() }
                <View style={styles.mainPageRow}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={ this.onCalculatorPress.bind(this) }>
                        <View style={styles.buttonStyle}>
                            <Animated.Image
                                style={styles.buttonBigImageStyle}
                                source={require('./assets/calculator-icon.png') } />
                            <Text style={styles.buttonBigTextStyle}>Calculator</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={[styles.mainPageRow]}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={this.onSubscribePress.bind(this) }
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.2)' }]}>
                        <View style={styles.buttonStyle}>
                            <Animated.Image
                                style={styles.buttonImageStyle}
                                source={require('./assets/subscribe-icon.png') } />
                            <Text style={styles.buttonTextStyle}>Subscribe</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={this.onAboutPress.bind(this) }
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.3)' }]}>
                        <View style={styles.buttonStyle}>
                            <Animated.Image
                                style={styles.buttonImageStyle}
                                source={require('./assets/about-icon.png') } />
                            <Text style={styles.buttonTextStyle}>About</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


MediaQueryStyleSheet.debug();
const styles = MediaQueryStyleSheet.create(
    ////Base Styles
    {
        mainPageStyle: {
            paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.MAIN_PAGE_BACKGROUND_COLOR,
        },
        mainPageRow: {
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'row',
            width: Style.DEVICE_WIDTH,
            marginTop: Style.PAGE_CONTENT_PADDING_TOP,
        },
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM * 3,
            paddingLeft: Style.MODAL_PADDING_LEFT,
            paddingRight: Style.MODAL_PADDING_RIGHT,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
        modalTitle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        modalTextStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT_REGULAR,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
            textAlign: Style.MODAL_ROW_TEXT_ALIGN,
            fontStyle: 'italic',
        },
        buttonStyle: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        buttonTextStyle: {
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 20,
            marginTop: 9,
            color: '#ffffff'
        },
        buttonImageStyle: {
            resizeMode: 'contain',
            width: 75,
            height: 75,
        },
        buttonBigImageStyle: {
            resizeMode: 'contain',
            width: 150,
            height: 150,
        },
        buttonBigTextStyle: {
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 28,
            marginTop: 9,
            color: '#ffffff'
        }
    },
    //Media Queries Styles
    {
        "@media (max-device-width: 320px)": {
            buttonImageStyle: {
                width: 50,
                height: 50,
            },
            buttonBigImageStyle: { 
                width: 100,
                height: 100,
            }
        },
    }
)

export default MainMenuPage;