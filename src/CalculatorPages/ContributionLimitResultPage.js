import React, { Component } from 'react';
import {Platform, PixelRatio, ScrollView, View, Text, TouchableOpacity} from 'react-native';
import DatePicker from 'react-native-datepicker';
import AppModal from '../AppModal.js';
import Style from '../Style.js';
import {getAge} from '../utils.js';
import {MediaQueryStyleSheet} from 'react-native-responsive';

class ContributionLimitResultPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            dateOfBirth: props.dateOfBirth,
            modalVisible: false,
        }
    }

    setModalVisible(b) {
        this.setState({ modalVisible: b });
    }

    onTablePress() {
        this.setModalVisible(true);
    }

    getContributionLimitInfo(age) {
        var contributionLimitInfo = {};
        if (age < 50) {
            contributionLimitInfo.concessional = {
                maximumAllowed: 30000,
                taxRatePercentage: 0.15,
            };
        } else if (age >= 50 && age <= 74) {
            contributionLimitInfo.concessional = {
                maximumAllowed: 35000,
                taxRatePercentage: 0.15,
            };
        } else {
            //NO CONTRIBUTIONS ALLOWED AFTER AGE 75
        }

        if (age <= 74) {
            contributionLimitInfo.nonConcessional = {
                maximumAllowed: 180000,
            };
        } else {
            //NO CONTRIBUTIONS ALLOWED AFTER AGE 75
        }

        return contributionLimitInfo;
    }

    renderMainResult(age, contributionLimitInfo) {
        const nonConcessionalBringForwardAmount = 540000;

        if (age >= 75) {
            return (
                <View style={styles.mainContentStyle}>
                    <Text style={[styles.mainContentTextStyle, { textAlign: 'center' }]}>No contributions allowed after <Text style={{ fontWeight: '800', color: '#385392' }}>age 75</Text> (except SGC) </Text>
                </View>);
        } else {
            return (<View style={styles.mainContentStyle}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.mainContentTextStyle}>Maximum <Text style={{ fontWeight: '800', color: '#385392' }}>concessional</Text> contribution</Text>
                    <Text style={styles.mainContentBigTextStyle}>{'$' + contributionLimitInfo.concessional.maximumAllowed.formatMoney(0) }</Text>
                    <Text style={[styles.mainContentTextStyle, { fontStyle: 'italic' }]}>(Tax Rate: {contributionLimitInfo.concessional.taxRatePercentage * 100 + '%'}) </Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 18 }}>
                    <Text style={styles.mainContentTextStyle}>Maximum <Text style={{ fontWeight: '800', color: '#385392' }}>non-concessional</Text> contributions</Text>
                    <Text style={styles.mainContentBigTextStyle}>{'$' + contributionLimitInfo.nonConcessional.maximumAllowed.formatMoney(0) }</Text>
                    {(age < 65) && <Text style={[styles.mainContentTextStyle, { fontWeight: '800', color: '#385392', marginTop: -9, marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_MARGIN_BOTTOM - 9 }]}>or</Text>}
                    {(age < 65) && <Text style={styles.mainContentBigTextStyle}>{'$' + nonConcessionalBringForwardAmount.formatMoney(0) }</Text>}
                    {(age < 65) && <Text style={styles.mainContentTextStyle}>3 year bring forward</Text>}
                    {(age <= 74 && age >= 65) && <Text style={[styles.mainContentTextStyle, { fontWeight: '800', color: '#385392' }]}>Only for Work Test</Text>}
                    {(age < 65) && <Text style={[styles.mainContentTextStyle, { fontStyle: 'italic' }]}>(Not Taxable) </Text>}
                </View>
            </View>);
        }
    }

    renderResult() {
        const age = getAge(this.state.dateOfBirth);
        var contributionLimitInfo = this.getContributionLimitInfo(age);

        return (
            <View style={styles.contentStyle}>
                {this.renderMainResult(age, contributionLimitInfo) }
                <View>
                    <View style={[styles.row, { backgroundColor: '#385392', justifyContent: 'center' }]}>
                        <Text style={{ fontSize: 18, color: '#ffffff' }}>Based on the following</Text>
                    </View>
                    <View style={[styles.row, { backgroundColor: '#f3f6fa' }]}>
                        <Text style={{ fontSize: 18, color: '#040b1e' }}>Date of Birth</Text>
                        <Text style={{ fontSize: 18, color: '#040b1e' }}>{this.state.dateOfBirth}</Text>
                    </View>
                    <Text style={{ marginTop: 18, fontSize: 16, fontStyle: 'italic', alignSelf: 'center', color: '#040b1e' }}>
                        For more information, refer to
                    </Text>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={this.onTablePress.bind(this) }>
                        <Text style={{ fontSize: 18, textDecorationLine: 'underline', fontWeight: '800', alignSelf: 'center', color: '#040b1e' }}>table</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderModal() {
        if (!this.state.modalVisible)
            return;

        return (
            <AppModal
                visible={this.state.modalVisible}
                onRequestClose={() => { this.setModalVisible(false) } }>
                <View style={styles.modalStyle}>
                    <Text style={styles.modalTitle}>Superannuation Contribution Annual Limits</Text>
                    <View style={[styles.modalRowStyle, styles.modalRowHeaderStyle]}>
                        <Text style={styles.modalRowHeaderText}>Concessional</Text>
                    </View>
                    <View style={[styles.modalRowStyle, styles.modalRowSubHeader]}>
                        <Text style={styles.modalRowSubHeaderText}>{"Age < 50 years"}</Text>
                    </View>
                    <View style={[styles.modalRowStyle]}>
                        <Text style={styles.modalRowTextStyle}>{"Max: $30,000"} </Text>
                        <Text style={styles.modalRowTextStyle}>Tax: 15% </Text>
                    </View>
                    <View style={[styles.modalRowStyle, styles.modalRowSubHeader]}>
                        <Text style={styles.modalRowSubHeaderText}>{"Age >= 50 years"}</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>{"Max: $35,000"} </Text>
                        <Text style={styles.modalRowTextStyle}>Tax: 15%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowHeaderText}>Non-concessional</Text>
                    </View>
                    <View style={[styles.modalRowStyle, styles.modalRowSubHeader]}>
                        <Text style={styles.modalRowSubHeaderText}>{"Age < 65 years"}</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>{"Max: $180,000 (or $540,000)*"}</Text>
                        <Text style={styles.modalRowTextStyle}>Tax: Nil</Text>
                    </View>
                    <View style={[styles.modalRowStyle, styles.modalRowSubHeader]}>
                        <Text style={styles.modalRowSubHeaderText}>{"Age 65 - 74 years (Work Test)"}</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>{"Max: $180,000"}</Text>
                        <Text style={styles.modalRowTextStyle}>Tax: Nil</Text>
                    </View>
                    <View style={[styles.modalRowStyle, styles.modalRowSubHeader]}>
                        <Text style={styles.modalRowSubHeaderText}>{"Age 65 - 74 years"}</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>Max: Nil</Text>
                        <Text style={styles.modalRowTextStyle}>Tax: Nil</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowHeaderText}>Small Business CGT</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>Max: $1.355 Million*</Text>
                        <Text style={styles.modalRowTextStyle}>Tax: Nil</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowHeaderText}>Gov.Co-contributions*</Text>
                    </View>
                    <View style={styles.modalRowStyle}>
                        <Text style={styles.modalRowTextStyle}>Max: No Limit</Text>
                        <Text style={styles.modalRowTextStyle}>Tax: Nil</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: '#ffffff', flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-start', marginTop: 18, height: 90 }]}>
                        <Text style={[styles.modalRowTextStyle, { fontSize: 12, fontStyle: 'italic' }]}>* 3 year bring forward</Text>
                        <Text style={[styles.modalRowTextStyle, { fontSize: 12, fontStyle: 'italic' }]}>{"* including max. $500,000 under CGT retirement exemption"}</Text>
                        <Text style={[styles.modalRowTextStyle, { fontSize: 12, fontStyle: 'italic' }]}>* Personal injury, W/Comp</Text>
                    </View>
                </View>
            </AppModal>
        );
    }

    render() {
        return (
            <View>
                {this.renderModal() }
                <ScrollView
                    style={{
                        flex: 1,
                        height: Style.DEVICE_HEIGHT,
                        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                        paddingTop: Style.PAGE_CONTENT_PADDING_TOP
                    }}
                    >
                    {this.renderResult() }
                </ScrollView>
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    //Base Styles
    {
        contentStyle: {
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            flexDirection: Style.PAGE_CONTENT_FLEX_DIRECTION,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM * 4,
        },
        row: {
            width: Style.DEVICE_WIDTH,
            height: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT / 4),
            flex: Style.PAGE_RESULT_ROW_FLEX,
            flexDirection: Style.PAGE_RESULT_ROW_FLEX_DIRECTION,
            alignItems: Style.PAGE_RESULT_ROW_ALIGN_ITEMS,
            justifyContent: Style.PAGE_RESULT_ROW_JUSTIFY_CONTENT,
            paddingLeft: Style.PAGE_RESULT_ROW_PADDING_LEFT,
            paddingRight: Style.PAGE_RESULT_ROW_PADDING_RIGHT,
        },
        mainContentStyle: {
            width: Style.DEVICE_WIDTH,
            justifyContent: Style.PAGE_RESULT_MAIN_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_RESULT_MAIN_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_RESULT_MAIN_CONTENT_BACKGROUND_COLOR,
            flex: Style.PAGE_RESULT_MAIN_CONTENT_FLEX,
            paddingTop: Style.PAGE_CONTENT_PADDING_BOTTOM,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM,
            paddingLeft: Style.PAGE_CONTENT_PADDING_X,
            paddingRight: Style.PAGE_CONTENT_PADDING_X,
        },
        mainContentTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_FONT_SIZE,
            color: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_MARGIN_BOTTOM,
            textAlign: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_ALIGN
        },
        mainContentBigTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_SIZE,
            fontWeight: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_WEIGHT,
            color: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_MARGIN_BOTTOM,
            textAlign: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_ALIGN,
        },
        modalRowTextStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
        modalRowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
        },
        modalRowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.25),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR
        },
        modalRowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
        modalTitle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        modalRowSubHeader: {
            backgroundColor: Style.MODAL_ROW_SUB_HEADER_BACKGROUND_COLOR,
            height: Style.MODAL_ROW_SUB_HEADER_HEIGHT
        },
        modalRowSubHeaderText: {
            color: Style.MODAL_ROW_SUB_HEADER_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_SIZE,
        },
        rowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT
        },
    },
    {
        "@media (max-device-width: 320px)": {
            modalRowTextStyle: {
                fontSize: (Platform.OS == 'android') ? 14 : 12,
            },
        },
    }
);

export default ContributionLimitResultPage;