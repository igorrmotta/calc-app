import React, { Component } from 'react';
import {ScrollView, Image, Modal, TouchableOpacity, View, Text} from 'react-native';
import AppModal from '../AppModal.js';
import Style from '../Style.js';
import {MediaQueryStyleSheet} from 'react-native-responsive';

class PensionAmountResultPage extends Component {
    constructor(props) {
        console.log(props);
        super(props);
        this.state = {
            navigator: props.navigator,
            result: props.result,
            modalVisible: false,
        }
    }

    setModalVisible(b) {
        this.setState({ modalVisible: b });
    }

    onTablePress() {
        this.setModalVisible(true);
    }

    renderResult() {
        return (
            <View style={styles.contentStyle}>
                <View style={styles.mainContentStyle}>
                    <Text style={styles.mainContentTextStyle}>Your estimated annual <Text style={{ fontWeight: '800', color: '#385392' }}>minimum</Text> payment</Text>
                    <Text style={styles.mainContentBigTextStyle}>{'$' + this.state.result.minimumAmount.formatMoney(0) }</Text>
                    {(this.state.result.bTTR) && <Text style={styles.mainContentTextStyle}>Your estimated annual <Text style={{ fontWeight: '800', color: '#385392' }}>maximum</Text> payment</Text>}
                    {(this.state.result.bTTR) && <Text style={styles.mainContentBigTextStyle}>{'$' + this.state.result.maximumAmount.formatMoney(0) }</Text>}
                </View>
                <View>
                    <View style={[styles.row, { backgroundColor: Style.PAGE_RESULT_ROW_HEADER_BACKGROUND_COLOR, justifyContent: Style.PAGE_RESULT_ROW_HEADER_JUSTIFY_CONTENT }]}>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_HEADER_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_HEADER_TEXT_COLOR }}>Based on the following</Text>
                    </View>
                    <View style={[styles.row, { backgroundColor: Style.PAGE_RESULT_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>Age Group</Text>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>{this.state.result.minPaymentInfo.age}</Text>
                    </View>
                    <View style={[styles.row, { backgroundColor: Style.PAGE_RESULT_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>Balance</Text>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>{'$' + this.state.result.balance.formatMoney(0) }</Text>
                    </View>
                    <View style={[styles.row, { backgroundColor: Style.PAGE_RESULT_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>Transition to Retirement</Text>
                        <Text style={{ fontSize: Style.PAGE_RESULT_ROW_TEXT_FONT_SIZE, color: Style.PAGE_RESULT_ROW_TEXT_COLOR }}>{this.state.result.bTTR ? 'Yes' : 'No'}</Text>
                    </View>
                    <View style={{ flex: 0, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ marginTop: 18, fontSize: 16, fontStyle: 'italic', alignSelf: 'center', color: '#040b1e' }}>
                            For more information, refer to
                        </Text>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={this.onTablePress.bind(this) }>
                            <Text style={{ fontSize: 18, textDecorationLine: 'underline', fontWeight: '800', alignSelf: 'center', color: '#040b1e' }}>table</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    renderModal() {
        if (!this.state.modalVisible)
            return;

        return (
            <AppModal
                visible={this.state.modalVisible}
                onRequestClose={() => { this.setModalVisible(false) } }>
                <View style={styles.modalStyle}>
                    <Text style={styles.modalTitle}>Minimum Annual Pension Payments</Text>
                    <View style={[styles.modalRowStyle, styles.modalRowHeaderStyle]}>
                        <Text style={styles.modalRowHeaderText}>Age Group</Text>
                        <Text style={ styles.modalRowHeaderText}>% of Balance</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>55-64</Text>
                        <Text style={styles.modalRowTextStyle}>4%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>65-74</Text>
                        <Text style={styles.modalRowTextStyle}>5%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>75-79</Text>
                        <Text style={styles.modalRowTextStyle}>6%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>80-84</Text>
                        <Text style={styles.modalRowTextStyle}>7%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>85-89</Text>
                        <Text style={styles.modalRowTextStyle}>9%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>90-94</Text>
                        <Text style={styles.modalRowTextStyle}>11%</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>95+</Text>
                        <Text style={styles.modalRowTextStyle}>14%</Text>
                    </View>
                </View>
            </AppModal>
        );
    }

    render() {
        return (
            <View>
                {this.renderModal() }
                <ScrollView
                    style=
                    {{
                        height: Style.DEVICE_HEIGHT,
                        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                        paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
                    }}>
                    {this.renderResult() }
                </ScrollView>
            </View>);
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM * 4,
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        row: {
            width: Style.DEVICE_WIDTH,
            height: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT / 4),
            flex: Style.PAGE_RESULT_ROW_FLEX,
            flexDirection: Style.PAGE_RESULT_ROW_FLEX_DIRECTION,
            alignItems: Style.PAGE_RESULT_ROW_ALIGN_ITEMS,
            justifyContent: Style.PAGE_RESULT_ROW_JUSTIFY_CONTENT,
            paddingLeft: Style.PAGE_RESULT_ROW_PADDING_LEFT,
            paddingRight: Style.PAGE_RESULT_ROW_PADDING_RIGHT,
        },
        mainContentStyle: {
            width: Style.DEVICE_WIDTH,
            backgroundColor: Style.PAGE_RESULT_MAIN_CONTENT_BACKGROUND_COLOR,
            paddingTop: Style.PAGE_RESULT_MAIN_CONTENT_PADDING_TOP,
        },
        mainContentTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_FONT_SIZE,
            color: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_MARGIN_BOTTOM,
            textAlign: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_ALIGN,
        },
        mainContentBigTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_SIZE,
            fontWeight: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_WEIGHT,
            color: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_MARGIN_BOTTOM,
            textAlign: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_ALIGN,
        },
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
        modalRowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.25),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR
        },
        modalRowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
        modalTitle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM,
            paddingLeft: Style.MODAL_PADDING_LEFT,
            paddingRight: Style.MODAL_PADDING_RIGHT,
            textAlign: Style.MODAL_ROW_TEXT_ALIGN,
        },
        modalRowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
        },
        modalRowTextStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
    }
);

export default PensionAmountResultPage;