import React, { Component } from 'react';
import {ScrollView, Text, View} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';
import Style from '../Style.js';

class TaxRatesTablePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    renderTable() {
        return (
            <View style={styles.contentStyle}>
                <View style={[styles.rowStyle, styles.rowHeaderStyle]}>
                    <Text style={[styles.textStyle, styles.rowHeaderText]}>Accumulation</Text>
                    <Text style={[styles.textStyle, styles.rowHeaderText]}>Pension</Text>
                </View>
                <View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>Concessional Contributions</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>15%</Text>
                    <Text style={styles.textStyle}>15%</Text>
                </View><View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>Net Investment Income</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>15%</Text>
                    <Text style={styles.textStyle}>Nil</Text>
                </View>
                <View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>Capital Gains</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>10%</Text>
                    <Text style={styles.textStyle}>Nil</Text>
                </View>
                <View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>Non-concessional Contributions</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>Nil</Text>
                    <Text style={styles.textStyle}>Nil</Text>
                </View>
                <View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>Government Contributions</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>Nil</Text>
                    <Text style={styles.textStyle}>Nil</Text>
                </View>
                <View style={[styles.rowStyle, styles.rowSubHeader]}>
                    <Text style={styles.rowSubHeaderText}>{"Non-arm's Lenght Income"}</Text>
                </View>
                <View style={styles.rowStyle}>
                    <Text style={styles.textStyle}>49%</Text>
                    <Text style={styles.textStyle}>49%</Text>
                </View>
            </View>);
    }

    render() {
        return (
            <View>
                <ScrollView
                    style={{
                        flex: 1,
                        height: Style.DEVICE_HEIGHT,
                        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                        paddingTop: Style.PAGE_CONTENT_PADDING_TOP,

                    }}>
                    {this.renderTable() }
                </ScrollView>
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            flexDirection: Style.PAGE_CONTENT_FLEX_DIRECTION,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM * 4
        },
        headerTitleStyle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM / 2,
            marginTop: Style.MODAL_TITLE_MARGIN_TOP / 2,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        rowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
            backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR
        },
        rowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.25),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR
        },
        textStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
        rowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
        rowSubHeader: {
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            height: Style.MODAL_ROW_SUB_HEADER_HEIGHT
        },
        rowSubHeaderText: {
            color: Style.MODAL_ROW_SUB_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_SIZE,
        },
    }
);

export default TaxRatesTablePage;