import React, { Component } from 'react';
import {ScrollView, TouchableOpacity, View, Text} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {IsValidDateOfBirth} from '../utils.js';
import Style from '../Style.js';

class PreservationAgeFormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            buttonDisabledState: true,
        }
    }

    onCalculatePress() {
        if (!this.state.dateOfBirth) {
            console.log("button shouldn't be enabled")
            return;
        }

        this.state.navigator.push({
            id: 'preservationAgeResult',
            title: 'Preservation Age',
            passProps: {
                dateOfBirth: this.state.dateOfBirth
            }
        });
    }

    onFieldChange(fieldName, value) {
        switch (fieldName) {
            case 'dateOfBirth':
                this.setState({ dateOfBirth: value }, function () {
                    this.setState({ buttonDisabledState: !this.checkFields() });
                });
                break;

            default:
                console.log('oops!');
                break;
        }
    }

    checkFields() {
        if (!IsValidDateOfBirth(this.state.dateOfBirth)) {
            return false;
        }

        return true;
    }

    renderForm() {
        return (
            <View style={styles.formStyle}>
                <View style={styles.fieldStyle}>
                    <Text style={styles.fieldTextStyle}>Date of Birth</Text>
                    <DatePicker
                        style={{ width: 200 }}
                        mode="date"
                        date={this.state.dateOfBirth}
                        placeholder="DD-MM-YYYY"
                        format="DD-MM-YYYY"
                        minDate="01-01-1900"
                        maxDate="01-01-2900"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={require('../assets/calendar-icon.png') }
                        onDateChange={(date) => { this.onFieldChange('dateOfBirth', date) } } />
                </View>
                <View style={styles.fieldStyle}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        disabled={this.state.buttonDisabledState}
                        style={[styles.fieldActionButtonStyle, (this.state.buttonDisabledState) ? { backgroundColor: 'rgba(21,21,21,0.3)' } : {}]}
                        onPress={this.onCalculatePress.bind(this) }>
                        <Text style={styles.fieldActionButtonTextStyle}> GO </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View>
                <ScrollView style={{
                    flex: 0,
                    paddingTop: Style.PAGE_CONTENT_PADDING_TOP
                }}>
                    {this.renderForm() }
                </ScrollView>
            </View>
        );
    }

}

const styles = {
    formStyle: {
        height: Style.PAGE_CONTENT_HEIGHT,
        justifyContent: Style.FORM_JUSTIFY_CONTENT,
        alignItems: Style.FORM_ALIGN_ITEMS,
        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
        flexDirection: Style.FORM_FLEX_DIRECTION,
    },
    fieldStyle: {
        marginTop: Style.FIELD_MARGIN_TOP,
        width: Style.FIELD_WIDTH,
        flex: Style.FIELD_FLEX,
        alignItems: Style.FIELD_ALIGN_ITEMS,
        justifyContent: Style.FIELD_JUSTIFY_CONTENT
    },
    fieldTextStyle: {
        fontFamily: Style.FIELD_TEXT_FONT_FAMILY,
        fontSize: Style.FIELD_TEXT_FONT_SIZE,
        color: Style.FIELD_TEXT_COLOR,
        fontWeight: Style.FIELD_TEXT_FONT_WEIGHT,
        marginBottom: Style.FIELD_TEXT_MARGIN_BOTTOM,
    },
    fieldActionButtonStyle: {
        width: Style.FIELD_ACTION_BUTTON_WIDTH,
        height: Style.FIELD_ACTION_BUTTON_HEIGHT,
        backgroundColor: Style.FIELD_ACTION_BUTTON_BACKGROUND_COLOR,
        borderRadius: Style.BORDER_RADIUS,
        flex: Style.FIELD_ACTION_BUTTON_FLEX,
        justifyContent: Style.FIELD_ACTION_BUTTON_JUSTIFY_CONTENT,
        alignItems: Style.FIELD_ACTION_BUTTON_ALIGN_ITEMS,
    },
    fieldActionButtonTextStyle: {
        color: Style.FIELD_ACTION_BUTTON_TEXT_COLOR,
        fontSize: Style.FIELD_ACTION_BUTTON_TEXT_FONT_SIZE,
        fontWeight: Style.FIELD_ACTION_BUTTON_TEXT_FONT_WEIGHT
    },
};

export default PreservationAgeFormPage;