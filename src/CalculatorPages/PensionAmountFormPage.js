import React, { Component } from 'react';
import {Platform, ScrollView, Navigator, BackAndroid, TouchableOpacity, View, Text, TextInput, Switch, Image} from 'react-native';
import Style from '../Style.js';
import DatePicker from 'react-native-datepicker';
import {round10, floor10, ceil10, getAge, convertStringDateToDateObject, IsValidDateOfBirth} from '../utils.js';

class PensionAmountsFormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            dateOfBirth: '',
            dateCommencement: new Date(),
            balance: '',
            bTTR: false,
            result: {},
            buttonDisabledState: true,
        };
    }

    getInformationOfMinimumPayment(age) {
        if (age >= 95) {
            return {
                age: '95+',
                percentage: 0.14
            }
        } else if (age >= 90) {
            return {
                age: '90-94',
                percentage: 0.11
            }
        } else if (age >= 85) {
            return {
                age: '85-89',
                percentage: 0.09
            }
        } else if (age >= 80) {
            return {
                age: '80-84',
                percentage: 0.07
            }
        } else if (age >= 75) {
            return {
                age: '75-79',
                percentage: 0.06
            }
        } else if (age >= 65) {
            return {
                age: '65-74',
                percentage: 0.05
            }
        } else if (age >= 55) {
            return {
                age: '55-64',
                percentage: 0.04
            }
        } else {
            //????????
            //Probably you are not allowed to have a pension yet!
            return {
                age: '55-',
                percentage: 0.04
            }
        }
    }

    getResult(commencementDate, bTTR, balance, minPaymentInfo) {
        var result = {};

        if (commencementDate.getMonth() <= 5) {
            console.log('before end of financial year');

            var minimumAmount;
            if (commencementDate.getMonth() === 5) {
                //but if start date is after 1 June, minimum pension is zero.
                minimumAmount = 0;
            } else {
                const commencementYear = commencementDate.getFullYear();
                const dateEndFinancialYear = new Date(commencementYear, 5, 30);

                const timeDiff = Math.abs(dateEndFinancialYear.getTime() - commencementDate.getTime());
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                const previousEndFinancialyear = new Date(commencementYear - 1, 5, 30);
                const timeDiffFinancialYear = Math.abs(dateEndFinancialYear.getTime() - previousEndFinancialyear.getTime());
                const diffDaysFinancialYear = Math.ceil(timeDiffFinancialYear / (1000 * 3600 * 24));

                const rate = diffDays / diffDaysFinancialYear;
                minimumAmount = balance * minPaymentInfo.percentage * rate;
            }

            var maximumAmount = balance * 0.1;

            //Rounded to the nearest $10
            minimumAmount = round10(minimumAmount, 1);

            //Rounded Down to the nearest 1$
            maximumAmount = Math.floor(maximumAmount);

            result.minimumAmount = minimumAmount;
            result.maximumAmount = maximumAmount;
            result.minPaymentInfo = minPaymentInfo;
            result.bTTR = bTTR;
            result.balance = balance;
        } else {
            console.log('after end of financial year');
            var minimumAmount = balance * minPaymentInfo.percentage;
            var maximumAmount = balance * 0.1;

            //Rounded to the nearest $10
            minimumAmount = round10(minimumAmount, 1);

            //Rounded Down to the nearest 1$
            maximumAmount = Math.floor(maximumAmount);

            result.minimumAmount = minimumAmount;
            result.maximumAmount = maximumAmount;
            result.minPaymentInfo = minPaymentInfo;
            result.bTTR = bTTR;
            result.balance = balance;
        }
        return result;
    }

    onCalculatePress() {
        if (!this.state.dateOfBirth || !this.state.balance) {
            //error, button shouldn't be enabled
            console.log("error, button shouldn't be enabled");
            return;
        }

        const age = getAge(this.state.dateOfBirth);
        const minPaymentInfo = this.getInformationOfMinimumPayment(age);
        const balance = parseFloat(this.state.balance);

        var commencementDate = convertStringDateToDateObject(this.state.dateCommencement);

        var result = this.getResult(commencementDate, this.state.bTTR, balance, minPaymentInfo);

        this.state.navigator.push({
            id: 'pensionAmountsResult',
            title: 'Pension Amounts',
            passProps: {
                result
            }
        });
    }

    checkFields() {
        if (!IsValidDateOfBirth(this.state.dateOfBirth)) {
            return false;
        }

        if (this.state.balance < 0 || !this.state.balance) {
            return false;
        }

        return true;
    }

    onFieldChange(fieldName, value) {
        switch (fieldName) {
            case 'dateOfBirth':
                this.setState({ dateOfBirth: value }, function () {
                    this.setState({ buttonDisabledState: !this.checkFields() });
                });
                break;

            case 'dateCommencement':
                this.setState({ dateCommencement: value });
                break;

            case 'balance':
                value = value.replace(/,/g, '');
                var floatBalance = parseFloat(value);
                this.setState({ strBalance: floatBalance.formatMoney(0) },
                    function () {
                        this.setState({ buttonDisabledState: !this.checkFields() });
                    });
                this.setState({ balance: floatBalance },
                    function () {
                        this.setState({ buttonDisabledState: !this.checkFields() });
                    });
                break;

            case 'bTTR':
                this.setState({ bTTR: value });
                break;

            default:
                console.log('oops!');
                break;
        }

    }

    renderForm() {
        return (
            <View style={styles.formStyle}>
                <View style={styles.fieldStyle}>
                    <Text style={styles.fieldTextStyle}>Date of Birth</Text>
                    <DatePicker
                        style={{ width: 200 }}
                        date={this.state.dateOfBirth}
                        mode="date"
                        placeholder="DD-MM-YYYY"
                        format="DD-MM-YYYY"
                        minDate="01-01-1900"
                        maxDate="01-01-2900"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={require('../assets/calendar-icon.png') }
                        onDateChange={(date) => { this.onFieldChange('dateOfBirth', date) } } />
                </View>
                <View style={styles.fieldStyle}>
                    <Text style={styles.fieldTextStyle}>Date Pension Commences</Text>
                    <DatePicker
                        style={{ width: 200 }}
                        date={this.state.dateCommencement}
                        mode="date"
                        placeholder="DD-MM-YYYY"
                        format="DD-MM-YYYY"
                        minDate="01-01-1900"
                        maxDate="01-01-2900"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource={require('../assets/calendar-icon.png') }
                        onDateChange={(date) => { this.onFieldChange('dateCommencement', date) } } />
                </View>
                <View style={styles.fieldStyle}>
                    <Text style={[styles.fieldTextStyle, { marginBottom: -5 }]}>Balance</Text>
                    {(Platform.OS === 'ios') && <TextInput style={styles.fieldTextInputStyleIOS}
                        onChangeText={(text) => { this.onFieldChange('balance', text) } }
                        placeholder="$"
                        keyboardType='numeric'
                        value={((this.state.strBalance) ? this.state.strBalance : '') }/>}
                    {(Platform.OS === 'android') && <TextInput style={styles.fieldTextInputStyle}
                        onChangeText={(text) => { this.onFieldChange('balance', text) } }
                        placeholder="$"
                        keyboardType='numeric'
                        value={((this.state.strBalance) ? this.state.strBalance : '') }
                        underlineColorAndroid='#385392'/>}
                </View>
                <View style={styles.fieldStyle}>
                    <Text style={styles.fieldTextStyle}>Transition to Retirement</Text>
                    <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        {(Platform.OS === 'ios') && <Switch
                            onValueChange={(value) => this.onFieldChange('bTTR', value) }
                            value={this.state.bTTR}
                            tintColor='#ffffff'
                            onTintColor='#051029'/>}
                        {(Platform.OS === 'android') && <Switch
                            onValueChange={(value) => this.onFieldChange('bTTR', value) }
                            value={this.state.bTTR}/>}
                        <Text style={[styles.fieldTextStyle, { marginLeft: 10, marginBottom: 0, fontWeight: '400' }]}>{this.state.bTTR ? 'Yes' : 'No'}</Text>
                    </View>
                </View>
                <View style={styles.fieldStyle}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        disabled={this.state.buttonDisabledState}
                        style={[styles.fieldActionButtonStyle, (this.state.buttonDisabledState) ? { backgroundColor: 'rgba(21,21,21,0.3)' } : {}]}
                        onPress={this.onCalculatePress.bind(this) }>
                        <Text style={styles.fieldActionButtonTextStyle}> GO </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View>
                <ScrollView style={{ flex: 1, height: Style.DEVICE_HEIGHT, backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR }}>
                    {this.renderForm() }
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    formStyle: {
        justifyContent: Style.FORM_JUSTIFY_CONTENT,
        alignItems: Style.FORM_ALIGN_ITEMS,
        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
        flexDirection: Style.FORM_FLEX_DIRECTION,
        paddingTop: Style.PAGE_CONTENT_PADDING_TOP + (Style.TOP_BAR_HEIGHT * 0.5),
        paddingBottom: Style.FORM_PADDING_Y * 2,
    },
    fieldStyle: {
        marginTop: Style.FIELD_MARGIN_TOP,
        width: Style.FIELD_WIDTH,
        flex: Style.FIELD_FLEX,
        alignItems: Style.FIELD_ALIGN_ITEMS,
        justifyContent: Style.FIELD_JUSTIFY_CONTENT
    },
    fieldTextStyle: {
        fontFamily: Style.FIELD_TEXT_FONT_FAMILY,
        fontSize: Style.FIELD_TEXT_FONT_SIZE,
        color: Style.FIELD_TEXT_COLOR,
        fontWeight: Style.FIELD_TEXT_FONT_WEIGHT,
        marginBottom: Style.FIELD_TEXT_MARGIN_BOTTOM,
    },
    fieldTextInputStyle: {
        width: 200,
        fontFamily: Style.FIELD_TEXT_FONT_FAMILY,
        fontSize: Style.FIELD_TEXT_FONT_SIZE,
        color: Style.FIELD_TEXT_COLOR,
    },
    fieldTextInputStyleIOS: {
        width: 200,
        fontFamily: Style.FIELD_TEXT_FONT_FAMILY,
        fontSize: Style.FIELD_TEXT_FONT_SIZE,
        color: Style.FIELD_TEXT_COLOR,
        backgroundColor: '#ffffff',
        marginTop: 9,
        flex: 1,
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 5,
        borderRadius: Style.BORDER_RADIUS,
    },
    fieldActionButtonStyle: {
        width: Style.FIELD_ACTION_BUTTON_WIDTH,
        height: Style.FIELD_ACTION_BUTTON_HEIGHT,
        backgroundColor: Style.FIELD_ACTION_BUTTON_BACKGROUND_COLOR,
        borderRadius: Style.BORDER_RADIUS,
        flex: Style.FIELD_ACTION_BUTTON_FLEX,
        justifyContent: Style.FIELD_ACTION_BUTTON_JUSTIFY_CONTENT,
        alignItems: Style.FIELD_ACTION_BUTTON_ALIGN_ITEMS,
    },
    fieldActionButtonTextStyle: {
        color: Style.FIELD_ACTION_BUTTON_TEXT_COLOR,
        fontSize: Style.FIELD_ACTION_BUTTON_TEXT_FONT_SIZE,
        fontWeight: Style.FIELD_ACTION_BUTTON_TEXT_FONT_WEIGHT
    },
};

export default PensionAmountsFormPage;