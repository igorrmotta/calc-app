import React, { Component } from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import Style from '../Style.js';

class DeathBenefitsTablePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    renderTabs() {
        return (
            <ScrollableTabView
                tabBarUnderlineColor={Style.TABS_SELECTED_TAB_COLOR}
                tabBarActiveTextColor={Style.PAGE_RESULT_ROW_HEADER_BACKGROUND_COLOR}
                style={{
                    flex: 1,
                    height: Style.DEVICE_HEIGHT,
                    backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                    paddingTop: Style.PAGE_CONTENT_PADDING_TOP
                }}
                tabBarTextStyle ={{ fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial' }}
                renderTabBar={() => <ScrollableTabBar />}
                >
                <ScrollView tabLabel='Lump Sum' >
                    <View tabLabel='Lump Sum' style={styles.contentStyle}>
                        <View style={[styles.rowStyle, styles.rowHeaderStyle, { backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR }]}>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Dependants</Text>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Non Dependants</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Tax Free Component</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Nil</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Taxable Component</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>17%</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Untaxed Element</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>32%</Text>
                        </View>
                    </View>
                </ScrollView>
                <ScrollView tabLabel='Pensions' >
                    <View tabLabel='Pensions' style={styles.contentStyle}>
                        <View style={[styles.rowStyle, styles.rowHeaderStyle]}>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Dependants</Text>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Non Dependants</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Tax Free Component</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Not Allowed</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Taxable Component</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={[styles.rowSubHeaderText, styles.rowSubHeaderTextIndented]}>Where Pensioner is 60 or above*</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Not Allowed</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={[styles.rowSubHeaderText, styles.rowSubHeaderTextIndented]}>Pensioner under 60</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <View style={styles.rowColumn}>
                                <Text style={[styles.textStyle]}>Beneficiary 60 or above</Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                            <View style={styles.rowColumn}>
                                <Text style={[styles.textStyle]}> </Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                        </View>
                        <View style={styles.rowStyle}>
                            <View style={styles.rowColumn}>
                                <Text style={[styles.textStyle]}>Beneficiary under 60</Text>
                                <Text style={styles.textStyle}>Normal Tax Rate Less 15%</Text>
                            </View>
                            <View style={styles.rowColumn}>
                                <Text style={[styles.textStyle]}> </Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                        </View>
                        <View style={[styles.rowStyle, { backgroundColor: '#ffffff', flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-start', height: 90, marginTop: 18, marginBottom: 36 }]}>
                            <Text style={[styles.modalRowTextStyle, { fontSize: 12, fontStyle: 'italic' }]}>* regardless of beneficiary age</Text>
                        </View>
                    </View>
                </ScrollView>
            </ScrollableTabView>
        );
    }

    render() {
        return (
            <View>
                {this.renderTabs() }
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            flexDirection: Style.PAGE_CONTENT_FLEX_DIRECTION,
            paddingBottom: Style.PAGE_CONTENT_PADDING_Y * 2,
        },
        headerTitleStyle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM / 2,
            marginTop: Style.MODAL_TITLE_MARGIN_TOP / 2,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        rowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
            backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR
        },
        textStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
        rowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
        rowSubHeader: {
            backgroundColor: Style.MODAL_ROW_SUB_HEADER_BACKGROUND_COLOR,
            height: Style.MODAL_ROW_SUB_HEADER_HEIGHT
        },
        rowSubHeaderText: {
            color: Style.MODAL_ROW_SUB_HEADER_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_SIZE,
        },
        rowSubHeaderTextIndented: {
            marginLeft: Style.MODAL_ROW_SUB_HEADER_INDENTED_TEXT_MARGIN_LEFT,
            fontStyle: Style.MODAL_ROW_SUB_HEADER_INDENTED_TEXT_FONT_STYLE,
            fontWeight: Style.MODAL_ROW_SUB_HEADER
        },
        rowColumn: {
            flex: Style.MODAL_ROW_COLUMN_FLEX,
            flexDirection: Style.MODAL_ROW_COLUMN_FLEX_DIRECTION
        },
        rowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.5),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR
        },
    },
    {
        "@media (max-device-width: 320px)": {
            rowHeaderText: {
                fontSize: 16,
            },
            rowSubHeaderText: {
                fontSize: 14,
            },
            textStyle: {
                fontSize: 14,
            }
        },
    }
);

export default DeathBenefitsTablePage;