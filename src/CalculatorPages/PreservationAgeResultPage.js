import React, { Component } from 'react';
import {ScrollView, Modal, View, Text, TouchableOpacity} from 'react-native';
import DatePicker from 'react-native-datepicker';
import AppModal from '../AppModal.js';
import Style from '../Style.js';
import {getAge, convertStringDateToDateObject} from '../utils.js';
import {MediaQueryStyleSheet} from 'react-native-responsive';

class PreservationAgeResultPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            dateOfBirth: props.dateOfBirth,
            modalVisible: false,
        }
    }

    setModalVisible(b) {
        this.setState({ modalVisible: b });
    }

    onTablePress() {
        this.setModalVisible(true);
    }

    getPreservationAge() {
        var dateOfBirth = convertStringDateToDateObject(this.state.dateOfBirth);
        var year = dateOfBirth.getFullYear();
        var month = dateOfBirth.getMonth();
        if (year < 1960 || (year === 1960 && month < 6)) {
            return 55;
        } else if ((year === 1960 && month >= 6) || (year === 1961 && month < 6)) {
            return 56;
        } else if ((year === 1961 && month >= 6) || (year === 1962 && month < 6)) {
            return 57;
        } else if ((year === 1962 && month >= 6) || (year === 1963 && month < 6)) {
            return 58;
        } else if ((year === 1963 && month >= 6) || (year === 1964 && month < 6)) {
            return 59
        } else if (year >= 1964 && month >= 6) {
            return 60;
        } else {
            //ooops!
        }

    }

    renderResult() {
        var age = this.getPreservationAge();

        return (
            <View style={styles.contentStyle}>
                <View style={styles.mainContentStyle}>
                    <Text style={[styles.mainContentTextStyle, { fontWeight: '800', color: '#385392' }]}>Preservation Age</Text>
                    <Text style={styles.mainContentBigTextStyle}>{age}</Text>
                </View>
                <View>
                    <View style={[styles.row, { backgroundColor: '#385392', justifyContent: 'center' }]}>
                        <Text style={{ fontSize: 18, color: '#ffffff' }}>Based on the following</Text>
                    </View>
                    <View style={[styles.row, { backgroundColor: '#f3f6fa' }]}>
                        <Text style={{ fontSize: 18, color: '#040b1e' }}>Date of Birth</Text>
                        <Text style={{ fontSize: 18, color: '#040b1e' }}>{this.state.dateOfBirth}</Text>
                    </View>
                    <Text style={{ marginTop: 18, fontSize: 16, fontStyle: 'italic', alignSelf: 'center', color: '#040b1e' }}>
                        For more information, refer to
                    </Text>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={this.onTablePress.bind(this) }>
                        <Text style={{ fontSize: 18, textDecorationLine: 'underline', fontWeight: '800', alignSelf: 'center', color: '#040b1e' }}>table</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderModal() {
        if (!this.state.modalVisible)
            return;

        return (
            <AppModal
                visible={this.state.modalVisible}
                onRequestClose={() => { this.setModalVisible(false) } }>
                <View style={styles.modalStyle}>
                    <Text style={styles.modalTitle}>Preservation Age</Text>
                    <View style={[styles.modalRowStyle, styles.modalRowHeaderStyle]}>
                        <Text style={[styles.modalRowTextStyle, styles.modalRowHeaderText]}>Date of Birth</Text>
                        <Text style={[styles.modalRowTextStyle, styles.modalRowHeaderText]}>Preservation Age</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>Before 1/7/60</Text>
                        <Text style={styles.modalRowTextStyle}>55</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>1/7/60 - 30/6/61</Text>
                        <Text style={styles.modalRowTextStyle}>56</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>1/7/61 - 30/6/62</Text>
                        <Text style={styles.modalRowTextStyle}>57</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>1/7/62 - 30/6/63</Text>
                        <Text style={styles.modalRowTextStyle}>58</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>1/7/63 - 30/6/64</Text>
                        <Text style={styles.modalRowTextStyle}>59</Text>
                    </View>
                    <View style={[styles.modalRowStyle, { backgroundColor: Style.MODAL_ROW_SECONDARY_BACKGROUND_COLOR }]}>
                        <Text style={styles.modalRowTextStyle}>After 30/6/64</Text>
                        <Text style={styles.modalRowTextStyle}>60</Text>
                    </View>
                </View>
            </AppModal>
        );
    }

    render() {
        return (
            <View>
                {this.renderModal() }
                <ScrollView
                    style={{
                        flex: 1,
                        height: Style.DEVICE_HEIGHT,
                        backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                        paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
                    }}>
                    {this.renderResult() }
                </ScrollView>
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            flexDirection: Style.PAGE_CONTENT_FLEX_DIRECTION,
            paddingBottom: Style.PAGE_CONTENT_PADDING_Y * 2,
        },
        row: {
            width: Style.DEVICE_WIDTH,
            height: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT / 4),
            flex: Style.PAGE_RESULT_ROW_FLEX,
            flexDirection: Style.PAGE_RESULT_ROW_FLEX_DIRECTION,
            alignItems: Style.PAGE_RESULT_ROW_ALIGN_ITEMS,
            justifyContent: Style.PAGE_RESULT_ROW_JUSTIFY_CONTENT,
            paddingLeft: Style.PAGE_RESULT_ROW_PADDING_LEFT,
            paddingRight: Style.PAGE_RESULT_ROW_PADDING_RIGHT,
        },
        mainContentStyle: {
            width: Style.DEVICE_WIDTH,
            justifyContent: Style.PAGE_RESULT_MAIN_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_RESULT_MAIN_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_RESULT_MAIN_CONTENT_BACKGROUND_COLOR,
            flex: Style.PAGE_RESULT_MAIN_CONTENT_FLEX,
            paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
            paddingBottom: Style.PAGE_CONTENT_PADDING_TOP,
        },
        mainContentTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_FONT_SIZE,
            color: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_MARGIN_BOTTOM,
        },
        mainContentBigTextStyle: {
            fontSize: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_SIZE,
            fontWeight: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_WEIGHT,
            color: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_COLOR,
            marginBottom: Style.PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_MARGIN_BOTTOM,
        },
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
        modalTitle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM,
        },
        modalRowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
        },
        modalRowTextStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
        modalRowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.25),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR
        },
        modalRowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
    },
    {
        "@media (max-device-width: 320px)": {
            modalRowHeaderText: {
                fontSize: 16,
            },
            modalRowTextStyle:{
                fontSize: 14,
            }
        },
    }
);

export default PreservationAgeResultPage;