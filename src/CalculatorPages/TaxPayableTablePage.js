import React, { Component } from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';
import {MediaQueryStyleSheet} from 'react-native-responsive';
import Style from '../Style.js';

class TaxPayableTablePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    renderTabs() {
        return (
            <ScrollableTabView
                tabBarUnderlineColor={Style.TABS_SELECTED_TAB_COLOR}
                tabBarActiveTextColor={Style.PAGE_RESULT_ROW_HEADER_BACKGROUND_COLOR}
                style={{
                    flex: 1,
                    height: Style.DEVICE_HEIGHT,
                    backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
                    paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
                }}
                tabBarTextStyle ={{ fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial' }}
                renderTabBar={() => <ScrollableTabBar />}>
                <ScrollView tabLabel='Lump Sum'>
                    <View style={styles.contentStyle}>
                        <View style={[styles.rowStyle, styles.rowHeaderStyle]}>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Tax Free Comp.</Text>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Taxable Comp.</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Under Preservation Age</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>22%</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Between Preservation Age and 59 Years</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <View style={styles.rowColumn}>
                                <Text style={styles.textStyle}>{"First $195,000"}</Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                            <View style={styles.rowColumn}>
                                <Text style={styles.textStyle}> </Text>
                                <Text style={styles.textStyle}>17%</Text>
                            </View>
                        </View>
                        <View style={styles.rowStyle}>
                            <View style={styles.rowColumn}>
                                <Text style={[styles.textStyle]}>Balance</Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                            <View style={styles.rowColumn}>
                                <Text style={styles.textStyle}> </Text>
                                <Text style={styles.textStyle}>Nil</Text>
                            </View>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Aged 60 and Above</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Nil</Text>
                        </View>
                    </View>
                </ScrollView>
                <ScrollView tabLabel='Pension' >
                    <View style={styles.contentStyle}>
                        <View style={[styles.rowStyle, styles.rowHeaderStyle]}>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Tax Free Comp.</Text>
                            <Text style={[styles.textStyle, styles.rowHeaderText]}>Taxable Comp.</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Between Preservation Age & 59 Years</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Normal tax rate less 15%</Text>
                        </View>
                        <View style={[styles.rowStyle, styles.rowSubHeader]}>
                            <Text style={styles.rowSubHeaderText}>Aged 60 or Above</Text>
                        </View>
                        <View style={styles.rowStyle}>
                            <Text style={styles.textStyle}>Nil</Text>
                            <Text style={styles.textStyle}>Nil</Text>
                        </View>
                    </View>
                </ScrollView>
            </ScrollableTabView>);
    }

    render() {
        return (
            <View>
                {this.renderTabs() }
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            backgroundColor: Style.PAGE_CONTENT_BACKGROUND_COLOR,
            flexDirection: Style.PAGE_CONTENT_FLEX_DIRECTION,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM * 2,
        },
        headerTitleStyle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM / 2,
            marginTop: Style.MODAL_TITLE_MARGIN_TOP / 2,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        rowStyle: {
            height: Style.MODAL_ROW_HEIGHT,
            width: Style.DEVICE_WIDTH,
            flex: Style.MODAL_ROW_FLEX,
            flexDirection: Style.MODAL_ROW_FLEX_DIRECTION,
            justifyContent: Style.MODAL_ROW_JUSTIFY_CONTENT,
            alignItems: Style.MODAL_ROW_ALIGN_ITEMS,
            paddingLeft: Style.MODAL_ROW_PADDING_LEFT,
            paddingRight: Style.MODAL_ROW_PADDING_RIGHT,
            backgroundColor: Style.MODAL_ROW_PRIMARY_BACKGROUND_COLOR
        },
        textStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
        },
        rowHeaderText: {
            color: Style.MODAL_ROW_HEADER_TEXT_COLOR,
            fontSize: Style.MODAL_ROW_HEADER_TEXT_FONT_SIZE,
            fontWeight: Style.MODAL_ROW_HEADER_TEXT_FONT_WEIGHT,
        },
        rowSubHeader: {
            backgroundColor: Style.MODAL_ROW_SUB_HEADER_BACKGROUND_COLOR,
            height: Style.MODAL_ROW_SUB_HEADER_HEIGHT
        },
        rowSubHeaderText: {
            color: Style.MODAL_ROW_SUB_HEADER_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_WEIGHT,
            fontSize: Style.MODAL_ROW_SUB_HEADER_TEXT_FONT_SIZE,
        },
        rowColumn: {
            flex: Style.MODAL_ROW_COLUMN_FLEX,
            flexDirection: Style.MODAL_ROW_COLUMN_FLEX_DIRECTION
        },
        rowHeaderStyle: {
            height: Style.MODAL_ROW_HEIGHT + (Style.MODAL_ROW_HEIGHT * 0.25),
            backgroundColor: Style.MODAL_ROW_HEADER_BACKGROUND_COLOR,
        },
    },
    {
        "@media (max-device-width: 320px)": {
            rowHeaderText: {
                fontSize: 16,
            },
            rowSubHeaderText: {
                fontSize: 14,
            },
            textStyle: {
                fontSize: 14,
            }
        },
    }
);

export default TaxPayableTablePage;