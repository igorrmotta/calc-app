//https://medium.com/@elieslama/responsive-design-in-react-native-876ea9cd72a8#.jlb8umumz

import {PixelRatio, Platform} from 'react-native';
import Dimensions from 'Dimensions';

// Precalculate Device Dimensions for better performance
const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

// Calculating ratio from iPhone breakpoints
const ratioX = x < 375 ? (x < 320 ? 0.75 : 0.875) : 1;
const ratioY = y < 568 ? (y < 480 ? 0.75 : 0.875) : 1;

// We set our base font size value
const base_unit = 16;

// We're simulating EM by changing font size according to Ratio
const unit = base_unit * ratioX;

// We add an em() shortcut function 
function em(value) {
  return unit * value;
}

const TOP_BAR_HEIGHT = (Platform.OS === 'ios') ? 64 : 54; //nav bar height48;
// Then we set our styles with the help of the em() function
export default Style = {

  // GENERAL
  DEVICE_WIDTH: x,
  DEVICE_HEIGHT: y,
  RATIO_X: ratioX,
  RATIO_Y: ratioY,
  UNIT: em(1),
  PADDING: em(1.25),
  BORDER_RADIUS: 5,
  BIG_BORDER_RADIUS: 8,

  // COVER
  CARD_WIDTH: x - em(1.25) * 2,
  CARD_HEIGHT: (x - em(1.25) * 2) * (3 / 5),
  CARD_PADDING_X: em(1.875),
  CARD_PADDING_Y: em(1.25),

  // FONT
  FONT_SIZE: em(1),
  FONT_SIZE_SMALLER: em(0.75),
  FONT_SIZE_SMALL: em(0.875),
  FONT_SIZE_TITLE: em(1.25),

  //TOP BAR
  TOP_BAR_WIDTH: x,
  TOP_BAR_HEIGHT: TOP_BAR_HEIGHT,
  TOP_BAR_BACKGROUND_COLOR: '#102352',
  TOP_BAR_ELEVATION: 10,
  TOP_BAR_FLEX: 0,
  TOP_BAR_JUSTIFY_CONTENT: 'center',
  TOP_BAR_ALIGN_ITEMS: 'center',

  //MAIN PAGE
  MAIN_PAGE_BACKGROUND_COLOR: '#385392',

  //PAGE CONTENT
  PAGE_CONTENT_HEIGHT: y - TOP_BAR_HEIGHT,
  PAGE_CONTENT_PADDING_TOP: TOP_BAR_HEIGHT,
  PAGE_CONTENT_PADDING_BOTTOM: 36,
  PAGE_CONTENT_PADDING_X: 30,
  PAGE_CONTENT_FLEX: 1,
  PAGE_CONTENT_JUSTIFY_CONTENT: 'center',
  PAGE_CONTENT_ALIGN_ITEMS: 'center',
  PAGE_CONTENT_BACKGROUND_COLOR: '#e7eaf0',
  PAGE_CONTENT_FLEX_DIRECTION: 'column',

  //PAGE RESULT
  PAGE_RESULT_MAIN_CONTENT_PADDING_TOP: 36,
  PAGE_RESULT_MAIN_CONTENT_JUSTIFY_CONTENT: 'center',
  PAGE_RESULT_MAIN_CONTENT_ALIGN_ITEMS: 'center',
  PAGE_RESULT_MAIN_CONTENT_BACKGROUND_COLOR: '#e7eaf0',
  PAGE_RESULT_MAIN_CONTENT_FLEX: 1,
  PAGE_RESULT_MAIN_CONTENT_TEXT_ALIGN: 'center',
  PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_FONT_SIZE: 18,
  PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_COLOR: '#040b1c',
  PAGE_RESULT_MAIN_CONTENT_TEXT_STYLE_MARGIN_BOTTOM: 18,
  PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_SIZE: 48,
  PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_FONT_WEIGHT: '800',
  PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_COLOR: '#385392',
  PAGE_RESULT_MAIN_CONTENT_BIG_TEXT_STYLE_MARGIN_BOTTOM: 18,
  PAGE_RESULT_ROW_FLEX: 0,
  PAGE_RESULT_ROW_FLEX_DIRECTION: 'row',
  PAGE_RESULT_ROW_ALIGN_ITEMS: 'center',
  PAGE_RESULT_ROW_JUSTIFY_CONTENT: 'space-between',
  PAGE_RESULT_ROW_PADDING_LEFT: 30,
  PAGE_RESULT_ROW_PADDING_RIGHT: 30,
  PAGE_RESULT_ROW_TEXT_FONT_SIZE: 18,
  PAGE_RESULT_ROW_TEXT_COLOR: '#040b1e',
  PAGE_RESULT_ROW_HEADER_BACKGROUND_COLOR: '#385392',
  PAGE_RESULT_ROW_HEADER_JUSTIFY_CONTENT: 'center',
  PAGE_RESULT_ROW_HEADER_TEXT_COLOR: '#ffffff',
  PAGE_RESULT_ROW_HEADER_TEXT_FONT_SIZE: 18,
  PAGE_RESULT_ROW_PRIMARY_BACKGROUND_COLOR: '#f3f6fa',
  PAGE_RESULT_ROW_SECONDARY_BACKGROUND_COLOR: '#ebedf2',

  //TABS
  TABS_SELECTED_TAB_COLOR: '#F6B300',

  //FORM
  FORM_JUSTIFY_CONTENT: 'flex-start',
  FORM_ALIGN_ITEMS: 'center',
  FORM_FLEX_DIRECTION: 'column',
  FORM_PADDING_Y: em(2),

  //FIELD
  FIELD_MARGIN_TOP: 36,
  FIELD_WIDTH: 200,
  FIELD_FLEX: 0,
  FIELD_ALIGN_ITEMS: 'center',
  FIELD_JUSTIFY_CONTENT: 'center',
  FIELD_TEXT_FONT_FAMILY: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
  FIELD_TEXT_FONT_SIZE: 16,
  FIELD_TEXT_COLOR: '#051029',
  FIELD_TEXT_FONT_WEIGHT: '800',
  FIELD_TEXT_MARGIN_BOTTOM: 9,
  FIELD_ACTION_BUTTON_WIDTH: 200,
  FIELD_ACTION_BUTTON_HEIGHT: 54,
  FIELD_ACTION_BUTTON_BACKGROUND_COLOR: '#ff9c00',
  FIELD_ACTION_BUTTON_FLEX: 0,
  FIELD_ACTION_BUTTON_JUSTIFY_CONTENT: 'center',
  FIELD_ACTION_BUTTON_ALIGN_ITEMS: 'center',
  FIELD_ACTION_BUTTON_TEXT_COLOR: '#ffffff',
  FIELD_ACTION_BUTTON_TEXT_FONT_SIZE: 20,
  FIELD_ACTION_BUTTON_TEXT_FONT_WEIGHT: '800',

  //MODAL
  MODAL_MARGIN_BOTTOM: 36,
  MODAL_PADDING_TOP: 18,
  MODAL_PADDING_BOTTOM: 18,
  MODAL_PADDING_LEFT: 30,
  MODAL_PADDING_RIGHT: 30,
  MODAL_BACKGROUND_COLOR: '#e7eaf0',
  MODAL_ELEVATION: 10,
  MODAL_FLEX: 0,
  MODAL_FLEX_DIRECTION: 'column',
  MODAL_ALIGN_ITEMS: 'center',
  MODAL_TITLE_TEXT_COLOR: '#040917',
  MODAL_TITLE_FONT_WEIGHT: '800',
  MODAL_TITLE_FONT_SIZE: 18,
  MODAL_TITLE_MARGIN_BOTTOM: 18,
  MODAL_TITLE_MARGIN_TOP: 18,
  MODAL_TITLE_TEXT_ALIGN: 'center',
  MODAL_ROW_HEIGHT: 54,
  MODAL_ROW_FLEX: 0,
  MODAL_ROW_FLEX_DIRECTION: 'row',
  MODAL_ROW_JUSTIFY_CONTENT: 'space-between',
  MODAL_ROW_ALIGN_ITEMS: 'center',
  MODAL_ROW_PADDING_LEFT: 30,
  MODAL_ROW_PADDING_RIGHT: 30,
  MODAL_ROW_TEXT_COLOR: '#040b1d',
  MODAL_ROW_TEXT_ALIGN: 'center',
  MODAL_ROW_TEXT_FONT_WEIGHT: '800',
  MODAL_ROW_TEXT_FONT_WEIGHT_REGULAR: '400',
  MODAL_ROW_TEXT_FONT_SIZE: 16,
  MODAL_ROW_HEADER_BACKGROUND_COLOR: '#385392',
  MODAL_ROW_HEADER_TEXT_COLOR: '#ffffff',
  MODAL_ROW_HEADER_TEXT_FONT_WEIGHT: '400',
  MODAL_ROW_HEADER_TEXT_FONT_SIZE: 22,
  MODAL_ROW_SUB_HEADER_BACKGROUND_COLOR: '#dae1f1',
  MODAL_ROW_SUB_HEADER_HEIGHT: 27,
  MODAL_ROW_SUB_HEADER_TEXT_COLOR: '#385392',
  MODAL_ROW_SUB_HEADER_TEXT_FONT_WEIGHT: '400',
  MODAL_ROW_SUB_HEADER_INDENTED_TEXT_MARGIN_LEFT: 15,
  MODAL_ROW_SUB_HEADER_INDENTED_TEXT_FONT_STYLE: 'italic',
  MODAL_ROW_SUB_HEADER_INDENTED_TEXT_FONT_WEIGHT: '400',
  MODAL_ROW_SUB_HEADER_TEXT_FONT_SIZE: 16,
  MODAL_ROW_PRIMARY_BACKGROUND_COLOR: '#ebeef3',
  MODAL_ROW_SECONDARY_BACKGROUND_COLOR: '#ffffff',
  MODAL_ROW_COLUMN_FLEX: 0,
  MODAL_ROW_COLUMN_FLEX_DIRECTION: 'column',
};