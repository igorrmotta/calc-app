import React, { Component } from 'react';
import {Platform, ScrollView, TouchableOpacity, View, Text, TextInput, Switch, Image} from 'react-native';
import Style, {em} from './Style.js';

import {MediaQueryStyleSheet} from 'react-native-responsive';

class CalculatorPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    onPesionAmountsPress() {
        this.state.navigator.push({
            id: 'pensionAmountsForm',
            title: 'Pension Amounts',
        });
    }

    onContributionLimitsPress() {
        this.state.navigator.push({
            id: 'contributionLimitsForm',
            title: 'Contribution Limits',
        });
    }

    onTaxRatesPress() {
        this.state.navigator.push({
            id: 'taxRatesTablePage',
            title: 'Tax Rates',
        });
    }

    onPreservationAgePress() {
        this.state.navigator.push({
            id: 'preservationAgeForm',
            title: 'Preservation Age',
        });
    }

    onTaxPayablePress() {
        this.state.navigator.push({
            id: 'taxPayableTablePage',
            title: 'Tax Payable',
        });
    }

    onDeathBenefitsPress() {
        this.state.navigator.push({
            id: 'deathBenefitsTablePage',
            title: 'Death Benefits',
        });
    }

    renderMenuOptions() {
        return (
            <View style={styles.contentStyle}>
                <View style={styles.row}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.1)' }]}
                        onPress={this.onPesionAmountsPress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Pension Amounts</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.2)' }]}
                        onPress={this.onContributionLimitsPress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Contribution Limits</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21,21, 0.3)' }]}
                        onPress={this.onTaxRatesPress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Tax Rates</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.4)' }]}
                        onPress={this.onPreservationAgePress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Preservation Age</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.5)' }]}
                        onPress={this.onTaxPayablePress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Tax Payable</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={[styles.buttonStyle, { backgroundColor: 'rgba(21, 21, 21, 0.6)' }]}
                        onPress={this.onDeathBenefitsPress.bind(this) }>
                        <Text style={styles.buttonTextStyle}>Death Benefits</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    render() {
        return (
            <View>
                <ScrollView style={{
                    backgroundColor: Style.MAIN_PAGE_BACKGROUND_COLOR,
                }}>
                    {this.renderMenuOptions() }
                </ScrollView>
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        contentStyle: {
            backgroundColor: Style.MAIN_PAGE_BACKGROUND_COLOR,
            height: Style.DEVICE_HEIGHT,
            flex: Style.PAGE_CONTENT_FLEX,
            justifyContent: Style.PAGE_CONTENT_JUSTIFY_CONTENT,
            alignItems: Style.PAGE_CONTENT_ALIGN_ITEMS,
            paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
        },
        row: {
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'row',
            width: Style.DEVICE_WIDTH,
        },
        buttonStyle: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#385392',
            paddingLeft: 15,
            paddingRight: 15,
        },
        buttonTextStyle: {
            color: '#ffffff',
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 20,
            textAlign: 'center',
        }
    },
    {
    }
);

export default CalculatorPage;