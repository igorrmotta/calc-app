import React, { Component } from 'react';
import {Platform, Image, View, PixelRatio, Text, TouchableOpacity, BackAndroid, Navigator} from 'react-native';
import Style from './Style.js';
import {MediaQueryStyleSheet} from 'react-native-responsive';

import MainMenuPage from './MainMenuPage.js';
import CalculatorPage from './CalculatorPage.js';
import PensionAmountsFormPage from './CalculatorPages/PensionAmountFormPage.js';
import PensionAmountsResultPage from './CalculatorPages/PensionAmountResultPage.js';
import ContributionLimitsFormPage from  './CalculatorPages/ContributionLimitFormPage.js';
import ContributionLimitsResultPage from  './CalculatorPages/ContributionLimitResultPage.js';
import PreservationAgeFormPage from './CalculatorPages/PreservationAgeFormPage.js';
import PreservationAgeResultPage from './CalculatorPages/PreservationAgeResultPage.js';
import TaxRatesTablePage from './CalculatorPages/TaxRatesTablePage.js';
import TaxPayableTablePage from './CalculatorPages/TaxPayableTablePage.js';
import DeathBenefitsTablePage from './CalculatorPages/DeathBenefitsTablePage.js';
import AboutPage from './AboutPage.js';

var NavigationBarRouteMapper = {
    LeftButton: function (route, navigator, index, navState) {
        if (index === 0) {
            return null;
        }

        return (
            <View style={styles.navBarButton}>
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => navigator.pop() }
                    style={[styles.navBarButton]}
                    >
                    <Image
                        style={{
                            resizeMode: 'contain',
                            height: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT * 0.7),
                            width: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT * 0.7)
                        }}
                        source={require('./assets/back-icon.png') }
                        />
                </TouchableOpacity>
            </View >
        );
    },

    Title: function (route, navigator, index, navState) {
        if (index === 0 && Platform.OS === 'ios') {
            return (
                <View style={styles.navBarButton}>
                    <Image
                        style={{
                            width: Style.DEVICE_WIDTH / 3,
                            height: Style.TOP_BAR_HEIGHT * 0.75,
                            resizeMode: 'contain',
                            paddingTop: 9,
                            paddingBottom: 9,

                        }}
                        source={require('./assets/white-logo.png') }/>
                </View>
            );
        } else {
            var title = (index === 0) ? 'Lighthouse Super' : route.title;
            return (
                <View style={styles.navBarButton}>
                    <Text style={styles.navBarText}>
                        {title}
                    </Text>
                </View>
            );
        }
    },

    RightButton: function (route, navigator, index, navState) {
        if (index === 0) {
            return (
                <TouchableOpacity
                    style={styles.navBarButton}
                    onPress={() => {
                        navigator.refs.mainMenuPage.setModalVisible(true);
                    } }>
                    <Image style={{
                        resizeMode: 'contain',
                        height: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT * 0.6),
                        width: Style.TOP_BAR_HEIGHT - (Style.TOP_BAR_HEIGHT * 0.6)
                    }}
                        source={require('./assets/disclaimer-icon.png') } />
                </TouchableOpacity>
            );
        } else
            return null;
    },
};

class AppNavigator extends Component {

    renderScene(route, navigator) {
        _navigator = navigator;
        switch (route.id) {
            case 'mainMenu':
                return (<MainMenuPage ref="mainMenuPage" navigator={navigator} {...route.passProps}/>);
            case 'calculator':
                return (<CalculatorPage navigator={navigator} title={route.title} {...route.passProps} />);
            case 'about':
                return (<AboutPage navigator={navigator} title={route.title} {...route.passProps} />);
            case 'pensionAmountsForm':
                return (<PensionAmountsFormPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'pensionAmountsResult':
                return (<PensionAmountsResultPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'contributionLimitsForm':
                return (<ContributionLimitsFormPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'contributionLimitsResult':
                return (<ContributionLimitsResultPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'preservationAgeForm':
                return (<PreservationAgeFormPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'preservationAgeResult':
                return (<PreservationAgeResultPage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'taxRatesTablePage':
                return (<TaxRatesTablePage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'taxPayableTablePage':
                return (<TaxPayableTablePage navigator={navigator} title={route.title} {...route.passProps}/>);
            case 'deathBenefitsTablePage':
                return (<DeathBenefitsTablePage navigator={navigator} title={route.title} {...route.passProps}/>);
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={{ id: 'mainMenu' }}
                renderScene={this.renderScene}
                navigationBar={<Navigator.NavigationBar
                    style={styles.navBar}
                    routeMapper={NavigationBarRouteMapper}/>}>
            </Navigator>
        );
    }
}

var _navigator; // we fill this up upon on first navigation.

BackAndroid.addEventListener('hardwareBackPress', () => {
    if (_navigator.getCurrentRoutes().length === 1) {
        return false;
    }
    _navigator.pop();
    return true;
});

const styles = MediaQueryStyleSheet.create(
    {
        navBar: {
            height: Style.TOP_BAR_HEIGHT,
            width: Style.TOP_BAR_WIDTH,
            backgroundColor: Style.TOP_BAR_BACKGROUND_COLOR,
            elevation: Style.TOP_BAR_ELEVATION,
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        navBarText: {
            fontSize: 18,
            color: '#ffffff',
            fontWeight: '500',
        },
        navBarButton: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            margin: 0,
            paddingLeft: 15,
            paddingRight: 15,
        },
        modalStyle: {
            width: Style.DEVICE_WIDTH,
            marginTop: Style.TOP_BAR_HEIGHT,
            marginBottom: Style.MODAL_MARGIN_BOTTOM,
            paddingTop: Style.MODAL_PADDING_TOP,
            paddingBottom: Style.MODAL_PADDING_BOTTOM * 3,
            paddingLeft: Style.MODAL_PADDING_LEFT,
            paddingRight: Style.MODAL_PADDING_RIGHT,
            backgroundColor: Style.MODAL_BACKGROUND_COLOR,
            elevation: Style.MODAL_ELEVATION,
            flex: Style.MODAL_FLEX,
            flexDirection: Style.MODAL_FLEX_DIRECTION,
            alignItems: Style.MODAL_ALIGN_ITEMS,
            borderRadius: Style.BIG_BORDER_RADIUS,
        },
        modalTitle: {
            color: Style.MODAL_TITLE_TEXT_COLOR,
            fontWeight: Style.MODAL_TITLE_FONT_WEIGHT,
            fontSize: Style.MODAL_TITLE_FONT_SIZE,
            marginBottom: Style.MODAL_TITLE_MARGIN_BOTTOM,
            textAlign: Style.MODAL_TITLE_TEXT_ALIGN,
        },
        modalTextStyle: {
            color: Style.MODAL_ROW_TEXT_COLOR,
            fontWeight: Style.MODAL_ROW_TEXT_FONT_WEIGHT_REGULAR,
            fontSize: Style.MODAL_ROW_TEXT_FONT_SIZE,
            textAlign: Style.MODAL_ROW_TEXT_ALIGN,
            fontStyle: 'italic',
        },
    },
    {}
)

export default AppNavigator;