import React, { Component } from 'react';
import {Platform, TouchableOpacity, Image, Animated, StyleSheet, Text, View} from 'react-native';
import Style from './Style.js';
import CalculatorIcon from './assets/calculator-icon.png';
import SubscribeIcon from './assets/subscribe-icon.png';

import {MediaQueryStyleSheet} from 'react-native-responsive';

class MainButton extends Component {
    constructor(props) {
        super(props);

        let fadeAnim = new Animated.Value(0);

        this.state = {
            onPress: props.onPress,
            fadeAnim: fadeAnim,
            text: props.text,
            img: props.img === "calculator" ? CalculatorIcon : SubscribeIcon,
        }
    }

    _onPressButton() {
        if (this.state.onPress)
            this.state.onPress();
    }

    componentDidMount() {
        Animated.timing(          // Uses easing functions
            this.state.fadeAnim,    // The value to drive
            { toValue: 1, duration: 1000 } // Configuration
        ).start();                // Don't forget start!
    }

    render() {

        return (
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.containterStyle}
                onPress={this._onPressButton.bind(this) }>
                <View style={styles.buttonStyle}>
                    <Animated.Image
                        style={[styles.imageStyle, { opacity: this.state.fadeAnim }]}
                        source={this.state.img} />
                    <Text style={styles.textStyle}>{this.state.text}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        containterStyle: {
            marginBottom: 36,
        },
        buttonStyle: {
            elevation: 5,
            flex: 0,
            justifyContent: 'center',
            alignItems: 'center',
        },
        textStyle: {
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 20,
            marginTop: 9,
        },
        imageStyle: {
            resizeMode: 'contain',
            width: 150,
            height: 150,
        },
    },
    //Media Queries Styles
    {
        "@media (max-device-width: 320px)": {
            imageStyle: {
                width: 100,
                height: 100,
            }
        },
    }
)

export default MainButton;