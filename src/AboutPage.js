import React, {Component} from 'react';
import {Platform, ScrollView, Image, Text, View} from 'react-native';
import {MediaQueryStyleSheet} from 'react-native-responsive';
import Style from './Style.js';

class AboutPage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: Style.MAIN_PAGE_BACKGROUND_COLOR }}>
                <View style={styles.page}>
                    <Image style={{ width: Style.DEVICE_WIDTH * 0.5, resizeMode: 'contain' }} source={require('./assets/white-logo.png') }/>
                    <View style={{
                        width: Style.DEVICE_WIDTH,
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                        <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.image} source={require('./assets/compliance-icon.png') }/>
                            <Text style={styles.textTitle}> Compliance </Text>
                        </View>
                        <Text style={[styles.text, { textAlign: 'left' }]}>
                            Generate all required financial statements and
                            electronically lodge your regular Business Activity Statements and
                            annual income tax return , to ensure that you are fully compliant
                            with all regulatory authorities.
                        </Text>
                    </View>
                    <View style={{
                        width: Style.DEVICE_WIDTH,
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                        <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.image} source={require('./assets/control-icon.png') }/>
                            <Text style={styles.textTitle}> Control </Text>
                        </View>
                        <Text style={[styles.text, { textAlign: 'right' }]}>
                            Access your SMSF data anywhere, anytime, to view the most up-to-date information.
                            Integrate planning and budget detail with your actuals, to see how your investments are tracking.
                        </Text>
                    </View>
                    <View style={{
                        width: Style.DEVICE_WIDTH,
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                        <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.image} source={require('./assets/share-icon.png') }/>
                            <Text style={styles.textTitle}> Share </Text>
                        </View>
                        <Text style={[styles.text, { textAlign: 'left' }]}>
                            At your discretion, share your data with your accountant, auditor, financial advisor and fellow trustees/members.
                        </Text>
                    </View>
                    <View style={{
                        width: Style.DEVICE_WIDTH,
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                        <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.image} source={require('./assets/connect-icon.png') }/>
                            <Text style={styles.textTitle}> Connect </Text>
                        </View>
                        <Text style={[styles.text, { textAlign: 'right' }]}>
                            Automatically connect with other data providers, including bank data feeds, sharebroker
                            transactions and market information, and the Australian Tax Office via SBR.
                        </Text>
                    </View>
                    <View style={{
                        width: Style.DEVICE_WIDTH,
                        flexDirection: 'column',
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        paddingLeft: 15,
                        paddingRight: 15,
                    }}>
                        <View style={{ flex: 0, alignItems: 'center', justifyContent: 'center' }}>
                            <Image style={styles.image} source={require('./assets/assurance-icon.png') }/>
                            <Text style={styles.textTitle}> Assurance </Text>
                        </View>
                        <Text style={[styles.text, { textAlign: 'left' }]}>
                            Our continuous monitoring and checking for possible compliance issues with instant warnings
                            will give you peace of mind.Rest with the knowledge that personal support is always
                            available and that you will be using Australian-designed, developed and supported cloud-based software.
                        </Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = MediaQueryStyleSheet.create(
    {
        page: {
            backgroundColor: Style.MAIN_PAGE_BACKGROUND_COLOR,
            paddingTop: Style.PAGE_CONTENT_PADDING_TOP,
            paddingBottom: Style.PAGE_CONTENT_PADDING_BOTTOM,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
        },
        textTitle: {
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 22,
            marginTop: 9,
            color: '#F6B300',
            fontWeight: '800',
        },
        text: {
            fontFamily: (Platform.OS === 'android') ? 'Roboto' : 'Arial',
            fontSize: 16,
            marginTop: 9,
            color: '#e7eaf0',
        },
        image: {
            marginTop: 36,
        }
    }
);

export default AboutPage;