import React, { Component } from 'react';
import {View, Image, Animated} from 'react-native';
import Style from './Style.js';

class CoverPage extends Component {
    constructor(props) {
        super(props);
        let fadeAnim = new Animated.Value(0);

        this.state = {
            fadeAnim: fadeAnim,
        }
    }

    componentDidMount() {
        Animated.timing(          // Uses easing functions
            this.state.fadeAnim,    // The value to drive
            { toValue: 1, duration: 1000 } // Configuration
        ).start();                // Don't forget start!
    }

    render() {
        var coverPageStyle = {
            width: Style.DEVICE_WIDTH,
            height: Style.DEVICE_HEIGHT,
            backgroundColor: '#051029',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        };

        return (
            <View style={coverPageStyle}>
                <Animated.Image
                    style={{
                        width: (Style.DEVICE_WIDTH - (2 * Style.CARD_PADDING_X)),
                        resizeMode: 'contain',
                        opacity: this.state.fadeAnim
                    }}
                    source={require('./assets/white-logo.png') }/>
            </View>
        );
    }
}

export default CoverPage;